"""Tree View Dialog Module."""
import os
from pathlib import Path

from PyQt6 import QtWidgets, uic


class TreeViewDialog(QtWidgets.QDialog):
    """Select containers in the Flywheel hierarchy."""

    def __init__(self, parent=None):
        """Initialize application."""
        super().__init__(parent)

        self.parent = parent
        self.source_dir = Path(os.path.dirname(os.path.realpath(__file__))).parent

        Form, _ = uic.loadUiType(self.source_dir / "resources/treeview.ui")
        self.ui = Form()
        self.ui.setupUi(self)
