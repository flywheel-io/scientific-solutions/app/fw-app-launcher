"""__summary__."""

import json
import logging
import os
import re
import shutil
from ast import literal_eval as ast_eval
from pathlib import Path

import nibabel as nib
import numpy as np
import pandas as pd
from PyQt6.QtWidgets import QInputDialog

log = logging.getLogger(__name__)


def start(app_name):
    """__summary__."""
    log.debug("Starting %s Segmentation use script", app_name)


def get_file_mod_times(path):
    """_summary_.

    Args:
        path (_type_): _description_

    Returns:
        _type_: _description_
    """
    file_mod_times = dict()
    len_path = len(str(path)) + 1
    for dp, dn, filenames in os.walk(path):
        for f in filenames:
            full_path = Path(dp) / f
            key = str(full_path)[len_path:]
            file_mod_times[key] = os.path.getmtime(full_path)
    return file_mod_times


def get_new_output_name(main_window, file_path):
    """Alter file_name to iterate.

    Ensures that an output name is not the same as an input name.

    Args:
        main_window (QApplication): Main Window of the application
        file_path (str): Relative path to output file with same name as an input

    Returns:
        str: Altered or iterated filepath
    """
    # grab the absolute path of the analysis root directory
    root_dir = main_window.analysis_management.analysis_dir

    # separate relative path to directory from the file name
    input_path = Path(file_path)
    input_dir = input_path.parent
    file_name = input_path.name

    # search for pattern of iterated filenames (e.g. file._2_.nii.gz)
    match_pat = re.search(r"\._\d+_\.", file_name)

    # if found find and iterate number...then rename the file accordingly
    if match_pat:
        match_num = re.search("\d+", match_pat.group())
        num = int(match_num.group())
        num += 1
        new_file_name = re.sub(r"\._\d+_\.", f"._{num}_.", file_name)
    # else, create the iterated file name
    else:
        new_file_name = (
            file_name.split(".")[0] + "._1_." + ".".join(file_name.split(".")[1:])
        )

    # TODO: Path validation on the result of this.
    #       May need to design a specific dialog to dynamically enforce filename
    #       validation.
    text, ok = QInputDialog.getText(
        main_window,
        "Output File Name Altered",
        f"<center><b>{file_name}</b> can not be the name of an input and an output.<br>"
        "Accept or alter the new name below:</center>",
        text=str(new_file_name),
    )

    # If the "OK" button was pushed, rename
    if ok:
        new_file_name = text
    # Else do not rename. Will cause a loop.
    else:
        new_file_name = file_name

    # If a different name, copy old file name to new file name.
    if str(new_file_name) != file_name:
        src = root_dir / input_dir / file_name
        dest = root_dir / input_dir / new_file_name
        shutil.copy(src, dest)

    return str(input_dir / new_file_name)


def before(application):
    """_summary_.

    Args:
        application (_type_): _description_
    """
    app_name = application.ui.AppName.text()
    log.debug("Before %s %s", app_name, application.app_management.use_script)

    # list the files that are present in the analysis to compare with what
    # is there afterwards
    file_mod_times = get_file_mod_times(application.analysis_management.analysis_dir)
    application.analysis_management.file_mod_times = file_mod_times


def itemize_segmentation_volumetrics(segmentation_path, output_path):
    """Create and save a csv file with all segmentation volumes.

    Args:
        segmentation_path (Pathlike): Source segmentation file.
        output_path (Pathlike): Output csv file with volumetrics.
    """
    try:
        nib_seg = nib.load(segmentation_path)
        seg_data = nib_seg.get_fdata()
        seg_data = seg_data.astype(int)
        seg_data = seg_data.flatten()
        seg_vol_df = pd.DataFrame(columns=["Label", "Voxel_Count", "Volume_mm3"])
        for i in np.unique(seg_data):
            row_dict = {}
            row_dict["Label"] = f"Label {i}"
            row_dict["Voxel_Count"] = np.count_nonzero(seg_data == i)
            row_dict["Volume_mm3"] = abs(
                row_dict["Voxel_Count"] * np.linalg.det(nib_seg.affine)
            )
            seg_vol_df = seg_vol_df.append(row_dict, ignore_index=True)
        seg_vol_df.to_csv(output_path, index=False)
        return ast_eval(seg_vol_df.to_json(orient="index"))

    except Exception as e:
        log.error(e)
        log.error("Failed to create segmentation volumetrics csv file.")
        return None


def after(application):
    """_summary_.

    Args:
        application (_type_): _description_
    """
    app_name = application.ui.AppName.text()
    analysis_id = application.analysis_management.gear_config["destination"]["id"]
    metadata = {
        "analysis": {
            "info": {
                "analysis_id": analysis_id,
            },
            "files": [],
            "tags": [app_name, analysis_id],
        },
    }
    output_dir = application.analysis_management.analysis_dir / "output"
    log.debug("After %s %s", app_name, application.app_management.use_script)

    # list the files in the analysis and move any files that were created or
    # update to the output
    old_mod_times = application.analysis_management.file_mod_times
    new_mod_times = get_file_mod_times(application.analysis_management.analysis_dir)

    files_to_put_in_output = list()
    for key, new_mod_time in new_mod_times.items():
        if key in old_mod_times:
            if old_mod_times[key] != new_mod_time:
                # Loop here to ensure output_name != input_name
                output_name = key
                # TODO: Validation could occur here and prompt user for a valid name
                while output_name == key:
                    output_name = get_new_output_name(application, key)
                files_to_put_in_output.append(output_name)
        else:
            files_to_put_in_output.append(key)

    for ff in files_to_put_in_output:
        changed_file_name = Path(ff).name
        old_place = application.analysis_management.analysis_dir / ff
        new_place = output_dir / changed_file_name
        os.rename(old_place, new_place)
        seg_info = itemize_segmentation_volumetrics(
            new_place, new_place.with_suffix(".csv")
        )
        if seg_info:
            metadata["analysis"]["files"].append(
                {
                    "name": new_place.name,
                    "info": {
                        "type": "segmentation",
                        "segmentation_info": seg_info,
                    },
                    "tags": ["segmentation", "volumetrics"],
                }
            )

    with open(output_dir / ".metadata.json", "w") as fff:
        json.dump(metadata, fff)
        log.info(f"Wrote {output_dir}/.metadata.json")


def stop(app_management):
    """_summary_.

    Args:
        app_management (_type_): _description_
    """
    app_name = app_management.main_window.ui.AppName.text()
    log.debug("Stopping %s %s", app_name, app_management.use_script)
