#!/usr/bin/env bash

if [ -f fw_app_launcher.spec ]; then
    echo "poetry run pyinstaller --noconfirm fw_app_launcher.spec "
    poetry run pyinstaller --noconfirm fw_app_launcher.spec 
else
    echo "poetry run pyinstaller (first time)"
    poetry run pyinstaller \
        -y \
        --windowed \
        --icon resources/flywheel.icns \
        --add-data apps:apps \
        --add-data resources:resources \
        --add-data pyproject.toml:. \
        --collect-all flywheel_gear_toolkit \
        --collect-all fw_meta \
        --collect-all fw_utils \
        --collect-all nibabel \
        --osx-bundle-identifier "io.flywheel.sse.fw_app_launcher" \
        --codesign-identity "Developer ID Application: Flywheel Exchange, LLC (8BJHB3C2GB)" \
        fw_app_launcher.py
fi
