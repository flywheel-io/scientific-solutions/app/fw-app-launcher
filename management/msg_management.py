"""Message Management Module."""
import logging
import sys

from PyQt6 import QtWidgets

log = logging.getLogger(__name__)


class MsgManagement:
    """Handle Messages."""

    def __init__(self, main_window):
        """Initialize Message object from Main Window.

        Args:
            main_window (QtWidgets.QMainWindow): [description]
        """
        super().__init__()
        log.debug("Init MsgManagement")
        self.main_window = main_window
        self.ui = main_window.ui
        self.msg = self.ui.plainTextEdit

        self.msg.setReadOnly(True)
