"""This file mimics the "run.py" file of a gear.

It provides the glue so fw_app_launcher can call a
stand-alone local application.
"""

import logging
import platform

from apps.Template.fw_template.main import run
from apps.Template.fw_template.parser import parse_config

log = logging.getLogger(__name__)


def start(app_name):
    """Do any necessary setup when this app is chosen by the user.

    This is called in app_management.py:choose_the_app() first thing after this module
    is loaded. Right now it only logs that it is being called.  For some future use
    case, it might be necessary.
    """
    log.debug("Starting %s", app_name)


def main(application):
    """Prepare command line arguments and launch the local application.

    This is called in run_gear() by fw_app_launcher.

    Returns:
        output from the local application and exit status
    """
    log.debug("In run.main()")
    params = parse_config(application.analysis_management.config_file)
    command = [
        application.app_management.manifest["custom"]["methods"]["Native_OS"]["Darwin"][
            "path"
        ]
    ]
    stdout, stderr, error_code = run(command, params)
    return stdout, stderr, error_code


def stop(app_management):
    """Do any necessary teardown when fw_app_launcher exits.

    This is called in two places: 1) fw_app_launcher.py when it closes and 2) after the
    local application is done if the "quit when done" check box is checked.

    Right now it only logs that it is called.

    Returns:
        Nothing
    """
    app_name = app_management.main_window.ui.AppName.text()
    log.debug("Stopping %s", app_name)
