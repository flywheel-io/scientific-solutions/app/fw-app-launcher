# Linux Installation

## Download

Download the latest version of the app from the [releases
page](https://drive.google.com/drive/u/1/folders/1iDlJh2BxDZ9JfUwwcc2U3wipBzuMNPfM).

## Install

Although this application will run in a "Stand-alone" mode, to use the web-launch
feature you will need to install the app with the following method.

1. Change directory to the location of the `bin/linux_extras/` folder.
2. From a command line, execute the following command: `./linux_install.sh <path to
downloaded linux executable>`

### ITK-SNAP

1. Download the latest version of ITK-SNAP from the [ITK-SNAP
website](http://www.itksnap.org/pmwiki/pmwiki.php?n=Downloads.SNAP4) for linux.
2. Decompress the downloaded file.
3. Copy the `bin` and `lib` folders from the decompressed folder to the `~/.local/`
folder.
4. Ensure that the `~/.local/bin` folder is in your `PATH` environment variable.

## Run

To run the application, execute the following command from a command line:
`fw-app-launcher`

## Web Launch

The web-launch feature is available through the kebab menue to the right-hand side of
the NIfTI file in Flywheel. If the Custom Protocol is not registered, you will be
prompted to register it. Once registered, you will be able to launch the application
from the kebab menu.

There may be some extra steps to follow for your browser to allow the application to
launch. Please see the [Web Launch](web_launch.md) page for more information.
