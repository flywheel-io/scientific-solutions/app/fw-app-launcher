"""Unit tests for running itk-snap"""
import os
from pathlib import Path

import flywheel_gear_toolkit
import pytest

from management.analysis_management import _upload_analysis_and_outputs

source_dir = Path(os.path.dirname(os.path.realpath(__file__))).parent
cache_dir = Path(source_dir / "tests/cache")


def test_upload_works(skip_message, install_assets):
    """ """

    msg = skip_message("ga")
    if msg != "":
        pytest.skip(msg)

    install_assets("upload_itk_snap_results.tgz")

    with flywheel_gear_toolkit.GearToolkitContext(input_args=[]) as gtk_context:
        analysis_dir = cache_dir / "626176972813cf2cfe9940fd"
        config_file = cache_dir / "626176972813cf2cfe9940fd/config.json"
        _upload_analysis_and_outputs(
            gtk_context.client,
            analysis_dir,
            config_file,
            "Test Analysis Name",
            None,
            None,
            None,
        )
    # assert: check that a new analysis was uploaded to
    # https://ga.ce.flywheel.io/#/projects/61dc586368722e06deaf3532/sessions/61dc58ec68722e06deaf37d2?tab=analyses
