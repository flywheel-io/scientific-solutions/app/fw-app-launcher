# fw-app-launcher

This is an application that can be installed on your computer that allows you to launch
local applications on your computer to access data stored on Flywheel. Currently,
the only application available is ITK-SNAP and fw_app_launcher works on macOS 11 (Big
Sur) and higher.  Windows and Linux are also supported. Addiitonal applications will be added in the
future.

The general idea is that an application has specific use cases and fw_app_launcher
provides code and instructions to enable users to accomplish these use cases. For
example, ITK-SNAP can be used to create segmentation masks on NIfTI or DICOM images.
The fw_app_launcher needs to retrieve files from Flywheel, launch ITK-SNAP, and then
upload the results. For a particular use case, the downloaded files need to be
provided to ITK-SNAP and the results of running ITK-SNAP need to be located and
uploaded to Flywheel. This is done using custom python scripts. The user needs to
understand what they should do with ITK-SNAP for this use case so instructions are
provided by custom web pages. The custom code and instructions are both necessary to
make the use case work because, for instance, if the user saves their segmentation file
in an unexpected place, the code will not be able to find it.

The fw_app_launcher can run any local application, but custom scripts and instructions
need to be created for each use case. An additional fake application, "TEMPLATE" is
provided to get help get started.

## Installation Instructions

### Install the Flywheel CLI

See [Installing the Flywheel Command–Line Interface
(CLI)](<https://docs.flywheel.io/hc/en-us/articles/360008162214-Installing-the-Flywheel-C>
ommand-Line-Interface-CLI-)

Then, using the CLI, log in to your Flywheel instance:

```Shell
fw login [your-API-key]
```

Alternatively, you can paste your api key into the "Flywheel Instance"
box in the fw-app-launcher SetUp dialog. Your api key will be saved
in the configuration file so you won't need to do this mor than
once.

### Install fw_app_launcher

#### macOS

1. Download fw_app_launcher.dmg
2. Double click to open
3. Drag the icon into the Applications folder

#### Windows

1. Download fw_app_launcher_installer.exe
2. Double click to open
3. Follow installation instructions

#### Linux

1. Download fw_app_launcher_linux
2. Set this file to be executable
3. Double click to run
   - This is a self-extracting executable that contains all of its own dependencies

### Install ITK-Snap (if not already installed)

<http://www.itksnap.org/pmwiki/pmwiki.php?n=Downloads.SNAP3>
(ITK-SNAP 3.8.0 works, previous versions may not. Windows requires ITK-SNAP 4.0.)

## Run fw_app_launcher

For running fw_app_launcher from the Flywheel platform directly, see below.

From your Applications folder double-click on the fw_app_launcher icon.

When you double-click on fw_app_launcher for the first time, if you
are not already logged in to a Flywheel instance, you will need to
provide your api key in the Setup dialog. If you already are logged in,
this Setup dialog will be skipped. You can access it any time via the
"Settings" button in the main window. If you have already run fw_app_launcher,
the Setup dialog will be skipped. Settings are stored in
`~/.config/flywheel/fw_app_launcher.json`.

Files that you want to use from your Flywheel instance will be
temporarily stored in a location on your local computer (the "Cache").
In the Setup dialog, you can set any path and name you like or keep
the default `/tmp/flywheel_cache`. If you have local applications you want to run using
fw_app_launcher, you can provide the path to them in "Apps Path".

After Setup, the next step is to select which local
application you want to run. For now, ITK-SNAP is the only choice that actually works.
After that, you need to choose a use case. Currently, the only used case is
Segmentation.

Before you can get to the files you want to segment, you'll need
to select a Flywheel group and project or else a collection. Then,
since you will be creating segmentation results, you need to decide
where to put those results in the Flywheel hierarchy (project,
subject, session, acquisition). The results will be placed in a new
"Analysis" container and you can attach this new container to any
container in the hierarchy.

To navigate the hierarchy, click where it says "**click to choose Analysis container
for results**". This will open a window where you'll see the name of the project and
folders at the project level. Folders can contain files or containers. The "SUBJECTS"
folder, not surprisingly contains subject containers. You can navigate down the
hierarchy by opening subjects, sessions, and acquisitions. At every level, you'll see
"FILES" and "ANALYSES" folders.

When you choose a project, subject, session, or acquisition container, your new
analysis container will be placed in its "ANALYSES" folder, and the segmentation mask
files saved by ITK-SNAP will be placed in the "FILES" folder in that new analysis
container. If you are segmenting files from multiple acquisitions, you may want to
attach your results to a session container. If you are segmenting a single file, you
might choose to attach the results to an acquisition container. You'll know you have
selected a valid container where your new analysis results will be saved when you see a
"Node ID" appear in the window. When you find the right container, click on the "OK"
button.

At this point, the display will change to show the type and name of the container you
have chosen. You can click it to change to a different container.

You'll notice that you can now click on the "Inputs", "Configuration", and
"Information" tabs. This is designed to mimic the "Run Gear" dialog on the Flywheel
platform. To select input files, click on a particular input and the Flywheel hierarchy
navigation window will appear again. Navigate to find the appropriate "FILES" folder.
The "g" input file for ITK-SNAP is the main image file. Hover over the "i" symbol for
more information about each input file. You will probably want to choose a file that
is attached to an acquisition container for this input. You'll know you have selected
a valid file when a "Node ID" apppears.

The "l" input file (that's lower case "L") is the ITK-SNAP label file. You will have
to provide this file and you probably want to upload it using the Flywheel platform and
attach it to the project. In this case, you'll find that file in the "FILES" folder
attached at the project level. ITK-SNAP will also let you use an existing segmentation
file and multiple additional image files. A previously created segmentation file might
be found in the "FILES" folder of an analysis container.

After you have chosen your input files in the "Inputs" tab and set any configuration
parameters in the "Configuration" tab, press the "Run Gear" button to launch ITK-SNAP.

At this point, you are the "human engine" running the gear. The fw_app_launcher will
patiently wait for you to finish. When you are done using ITK-SNAP, (don't forget to
save your segmentation), just quit ITK-SNAP and control will return to the
fw_app_launcher. The fw_app_launcher will then upload the results to Flywheel.

The custom python code that is run before and after launching ITK-SNAP expects that you
will save the segmentation in the default place. That is, don't save the file on the
desktop or some other place. Just select "Save Segmentation Image..." from the
"Segmentation" menu (or press command-s). You can name the file whatever you want and
then press "Finish". Don't press "Browse../" to save the file in another location. If
you are editing an existing segmentation file, it will already have a name so you can
just press "Finish" or else give it a new name and press "Finish".

By default, fw_app_launcher will quit after you quit ITK-SNAP but not before saving the
new analysis on the container you chose, and also uploading the result segmentation
file(s). Yes, you can create multiple segmentation files and they will be uploaded as
well.

You can now go to the Flywheel platform and navigate to the proper project, subject,
etc. and you will be able to find the analysis that you just created along with the
result segmentation(s).

## Run fw_app_launcher from the Flywheel Web UI

Clicking on the triple vertical dot "kebab" menu can launch ITK-SNAP on a particular
NIfTI or DICOM file:
![Kebab menu](docs/img/Kebab_menu.png "Kebab menu")
Select "View in ITK-SNAP":
![View in ITK-SNAP](docs/img/View_in_ITK-SNAP.png "View in ITK-SNAP")

Doing this will cause a URL to be generated that will be interpreted by your browser to
open the fw_app_launcher application on your local machine.  The fw_app_launcher will
use the contents of the URL to download the selected file from Flywheel and open it in
ITK-SNAP.  You can create a segmentation and when you save it and quite ITK-SNAP, the
segmentation file will be uploaded to a newly created analysis on the container where
the file was found.  Select the "Analyses" tab in the Flywheel UI to see the new
analysis.

The menu item shown above will appear when a Flywheel Extension SDK application is
installed (See [Applications](https://docs.flywheel.io/hc/en-us/articles/4403390314003)
documentation).

To set up an application to launch ITK-SNAP via the fw_app_launcher, add an application
like this:
![Flywheel Extension App](docs/img/Flywheel_Extension_App.png "Flywheel Extension App")

- The "Type" should be "Viewer"
- Give it a name like "ITK-SNAP"
- The URL is <https://cdn.flywheel.io/apps/uri-launcher/latest/index.html>
- Give it some sort of description
- Set the types of files to match as "dicom" and "nifti"
- Use this as the "Application options"

```json
{
  "uriTemplate":
  "fwlaunch://ITK_SNAP?use=URL&destination=<%= 
  it.container._id %>&file_name=<%= encodeURIComponent(it.file.name) %>"
}
```

Where, in the above code:

- "fwlaunch" means send the URL to the fw_app_launcher app installed on your local
computer.
- "ITK_SNAP" selects the app to launch
- "use=URL" selects the "URL" use case
- it.container._id will become the container that the file is in, and
- it.file.name will be the name of the file

You can change "ITK_SNAP" and "URL" to have the fw_app_launcher launch a different
local app and use case.

## Contributing

For more information about how to get started contributing to this project,
see [CONTRIBUTING.md](CONTRIBUTING.md).
