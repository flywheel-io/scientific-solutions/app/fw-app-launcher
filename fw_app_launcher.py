#!/usr/env python3
"""fw_app_launcher!"""
import argparse
import logging
import os
import sys
from pathlib import Path

import toml
from PyQt6 import QtGui, QtWidgets
from PyQt6.QtCore import QEvent, QUrl

from management.app_launcher_window import AppLauncher, FileLaunchEvent


class fw_application(QtWidgets.QApplication):
    """Main Application."""

    def __init__(self, args):
        """Get URL."""
        super().__init__(args)
        self.args = args
        cli_args = self.parse_command_line()
        self.url = QUrl(cli_args.url) if cli_args.url else None
        self.url_messages = []
        self.applauncher = None
        self.file_open_event = None
        # if the URL is valid, send a FileLaunchEvent to the application
        if self.url:
            event = FileLaunchEvent(self.url)
            QtWidgets.QApplication.sendEvent(self, event)

    def parse_command_line(self):
        """Parse command line arguments.

        Returns:
            args: Parsed Command Line Arguments
        """
        parser = argparse.ArgumentParser(description=__doc__)
        parser.add_argument(
            "-u",
            "--url",
            type=str,
            help="The fully qualified URL of a web-launch (e.g. --url=fwlaunch://ITK_SNAP/?use=URL...)",
            default=None,
        )
        return parser.parse_args()

    def event(self, e: QEvent):
        """Handle macOS FileOpen events.

        Args:
            e (QEvent): Event that is being handled

        Returns:
            bool: Was the event handled?
        """
        event_type = e.type()
        if self.applauncher:
            self.applauncher.ui.plainTextEdit.appendPlainText(str(event_type))
        # QFileOpenEvent is a MacOS-only event
        if event_type == QEvent.Type.FileOpen:
            self.url = e.url()
            if self.url.isValid():
                self.url_messages.append("Found valid URL " + self.url.toString())
                if self.applauncher:
                    self.applauncher.run_via_url(self.url)
                else:
                    self.file_open_event = e
            else:
                self.url_messages.append("Found invalid URL " + self.url.toString())
            if self.applauncher:
                self.applauncher.ui.plainTextEdit.appendPlainText(
                    str(self.url_messages)
                )
                self.url_messages = []
        elif event_type == QEvent.Type.ApplicationStateChange:
            # if self.file_open_event:
            #    self.applauncher.run_via_url(self.file_open_event.url)
            event_type = event_type
        else:
            return super().event(e)
        return True


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(name)s - [%(filename)s:%(lineno)d] - %(levelname)s - %(message)s",
    )
    log = logging.getLogger(__name__)
    log.info("Starting up")

    # Get version from pyproject.toml
    with open(Path(__file__).parent / "pyproject.toml", "r") as f:
        data = toml.load(f)
        version_string = data["tool"]["poetry"]["version"]
        log.info("Version: " + version_string)

    log.info(sys.argv)

    source_dir = Path(os.path.dirname(os.path.realpath(__file__)))

    app = fw_application(sys.argv)
    app.setWindowIcon(QtGui.QIcon(str(source_dir / "resources/Flywheel.icns")))
    app.setApplicationName("Flywheel App Launcher")
    app.setApplicationVersion(version_string)
    application = AppLauncher(app)
    app.applauncher = application
    if app.file_open_event:
        if len(app.url_messages) > 0:
            app.applauncher.ui.plainTextEdit.appendPlainText("Found saved URL event")
            for msg in app.url_messages:
                app.applauncher.ui.plainTextEdit.appendPlainText(msg)
        application.run_via_url(app.url)

    if not app.file_open_event:
        application.show()

        return_val = app.exec()

        application.quit_fw_app_launcher(return_val)
